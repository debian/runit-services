Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: README
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
           2022-2025 Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: update-copyright
Copyright: 2022-2024 Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: debian/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
           2022-2025 Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: sv/acpi-fakekey/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/anacron/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/apache2/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/atd/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/avahi-daemon/*
Copyright: 2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/avahi-dnsconfd/*
Copyright: 2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/bluetooth/*
Copyright: 2022 Cloux <cloux@rote.ch>
            2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0
Comment: adapted to Debian from https://github.com/cloux/runit-base

Files: sv/chrony/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
           2022 Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: sv/connman/*
Copyright: 2023 Dimitris <dimitris@stinpriza.org>
            2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0
Comment: slightly changed from the one proposed in #1032657

Files: sv/cron/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
           2022 Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: sv/cups/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/dbus.dep-fixer/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/dbus/*
Copyright: 2022-2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/dhclient/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
             2022-2023  Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: sv/docker/*
Copyright: 2019-2024 cloux <jan@wespe.dev>
                  2024 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/elogind/*
Copyright: 2022-2024 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/exim4/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
           2024 Andrew Bower <andrew@bower.uk>
License: CC0-1.0

Files: sv/gdomap/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/gpm/*
Copyright: 2023 Friedhelm Mehnert <friedhelm@friedhelms.net>
License: GPL-2.0+
Comment: CFG parsing is from Debian's /etc/init.d/gpm

Files: sv/haveged/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/isc-dhcp-server.6/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
        2023 Lorenzo Puliti <plorenzo@disroot.org>
        2024 Andrew Bower <andrew@bower.uk>
License: BSD-3-Clause

Files: sv/isc-dhcp-server/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
        2023 Lorenzo Puliti <plorenzo@disroot.org>
        2024 Andrew Bower <andrew@bower.uk>
License: BSD-3-Clause

Files: sv/lightdm/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/lighttpd/*
Copyright: 2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/lircd/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/lldpd/*
Copyright: 2024 Andrew Bower <andrew@bower.uk>
License: BSD-3-Clause

Files: sv/mariadb/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/mdadm/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/mini-httpd/*
Copyright: 2017 Dmitry Bogatov <KAction@disroot.org>
             2023  Lorenzo Puliti <plorenzo@disroot.org>
License: GPL-3+

Files: sv/mpd/*
Copyright: 2023 Friedhelm Mehnert <friedhelm@friedhelms.net>
License: CC0-1.0

Files: sv/network-manager/*
Copyright: 2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/nginx/*
Copyright: 2019 Cloux <cloux@rote.ch>
             2023  Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/openntpd/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/postfix/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/preload/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/proftpd/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/rsyslog/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/sddm/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/slim/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/smartmontools/*
Copyright: 2023-2024 Dimitris T <dimitris@stinpriza.org>
             2024  Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/unbound/*
Copyright: 2024 Dimitris T. <dimitris@stinpriza.org>
License: BSD-3-Clause

Files: sv/uuidd/*
Copyright: 2022 Cloux <cloux@rote.ch>
            2023 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0
Comment: adapted to Debian from https://github.com/cloux/runit-base

Files: sv/wicd/*
Copyright: 2022 Lorenzo Puliti <plorenzo@disroot.org>
License: CC0-1.0

Files: sv/xdm/*
Copyright: 2005-2008 Gerrit Pape <pape@smarden.org>
       2022 Lorenzo Puliti <plorenzo@disroot.org>
License: BSD-3-Clause

Files: sv/zcfan/*
Copyright: 2023 Martin Steigerwald <martin@lichtvoll.de>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in ‘/usr/share/common-licenses/CC0-1.0’.

License: GPL-2.0+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On a Debian system, you can find the full text of the GNU General
 Public License, version 2 in /usr/share/common-licenses/GPL-2.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
