Source: runit-services
Section: admin
Priority: optional
Maintainer: Lorenzo Puliti <plorenzo@disroot.org>
Build-Depends: debhelper-compat (= 13), dh-sequence-runit, dh-runit (>=2.15.2)
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/runit-services
Vcs-Git: https://salsa.debian.org/debian/runit-services.git
Rules-Requires-Root: no

Package: runit-services
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends},runit (>= 2.1.2-56), runit-helper (>= 2.16.3)
Breaks: ${runit:Breaks}, elogind (<< 255.4.1-1~)
Recommends: runit-run | runit-init
Provides: mini-httpd-run
Suggests: socklog, xchpst
Description: UNIX init scheme with service supervision (services)
 runit is a replacement for SysV-init and other init schemes.  It runs on
 Debian GNU/Linux, *BSD, MacOSX, and Solaris, and may be easily adapted
 to other Unix operating systems.  runit implements a simple three-stage
 concept.  Stage 1 performs the system's one-time initialization tasks.
 Stage 2 starts the system's uptime services (via the runsvdir program).
 Stage 3 handles the tasks necessary to shutdown and halt or reboot.
 .
 See http://smarden.org/runit/ for more information.
 .
 This package contains services directories for a collection of services
 available in Debian, to have them run under runit service supervision
 instead of sysvinit or systemd. Services directories are installed under
 /usr/share/runit/sv/. Local copies of services can be maintained easily
 in /etc/sv/ with the cpsv utility that can be used to copy, inspect and
 update runscripts in /etc/sv/ after the first installation.
 It's recommended to read
 https://salsa.debian.org/debian/runit-services/-/blob/master/README
 before installing this package.
